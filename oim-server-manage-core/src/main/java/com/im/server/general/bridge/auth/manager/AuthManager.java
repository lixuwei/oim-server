package com.im.server.general.bridge.auth.manager;

import java.util.List;

/**
 * date 2018-06-13 10:41:23<br>
 * description
 * 
 * @author XiaHui<br>
 * @since
 */

public interface AuthManager {

	public String createToken(String userId);

	public void putToken(String userId, String token);

	public boolean isAuth(String token);

	public String getToken(String userId);

	public String getUserId(String token);

	public String removeByUserId(String userId);

	public String removeByToken(String token);

	public boolean hasPermission(String token, String permissionKey);
	
	public void setPermission(String token, List<String> permissionKeyList);
}
