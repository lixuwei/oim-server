package com.im.server.general.common.data.system;

import java.util.List;

/**
 * date 2018-07-12 10:22:36<br>
 * description
 * 
 * @author XiaHui<br>
 * @since
 */
public class MenuQuery {

	private String queryText;
	private String id;// 菜单id
	private String superId;// 上级菜单id
	private String name;// 菜单名称
	private String flag;// 是否启用 1：启用 0：停用
	private int type;// 菜单类别 0：菜单 1：按钮
	private List<String> idList;// 条件包含的id
	private List<String> outIdList;// 条件排除的id

	public String getQueryText() {
		return queryText;
	}

	public void setQueryText(String queryText) {
		this.queryText = queryText;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSuperId() {
		return superId;
	}

	public void setSuperId(String superId) {
		this.superId = superId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public List<String> getIdList() {
		return idList;
	}

	public void setIdList(List<String> idList) {
		this.idList = idList;
	}

	public List<String> getOutIdList() {
		return outIdList;
	}

	public void setOutIdList(List<String> outIdList) {
		this.outIdList = outIdList;
	}

}
