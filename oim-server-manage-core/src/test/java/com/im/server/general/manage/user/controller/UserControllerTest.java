package com.im.server.general.manage.user.controller;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import com.im.server.general.common.bean.User;
import com.onlyxiahui.common.lib.util.HttpClient3Util;
import com.onlyxiahui.common.lib.util.OnlyJsonUtil;
import com.onlyxiahui.common.message.request.RequestMessage;

public class UserControllerTest {

	String baseUrl = "http://127.0.0.1:12000";

	@Test
	public void list() {
		String url = baseUrl + "/manage/user/list";
		RequestMessage rm = new RequestMessage();
		String data = OnlyJsonUtil.objectToJson(rm);
		Map<String, String> map = new HashMap<String, String>();
		map.put("data", data);
		String json = HttpClient3Util.post(url, map);
		System.out.println(json);
	}
	
	@Test
	public void add() {
		String url = baseUrl + "/manage/user/add";

		User user = new User();
		user.setId("444444");
		user.setNumber(0);
		user.setAccount("100086");
		user.setPassword("123456");
		user.setType("1");

		RequestMessage rm = new RequestMessage();
		rm.put("user", user);
		
		String data = OnlyJsonUtil.objectToJson(rm);
		Map<String, String> map = new HashMap<String, String>();
		map.put("data", data);
		String json = HttpClient3Util.post(url, map);
		System.out.println(json);
	}
}
